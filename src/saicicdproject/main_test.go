package main

import "testing"

func TestHelloWorldString(t *testing.T){
  if getHelloWorldString() != "Warm welcome to Teched-2017!"{
    t.FailNow()
  }
}
